"use strict";

let num1=+prompt("Enter first number: ");
while (!Number(num1)) {
    num1 = prompt("ERROR not number! Please, enter first number again! "+ num1);
}

let num2=+prompt("Enter second number: ");
while (!Number(num2)) {
    num2 = prompt("ERROR not number! Please, enter second number again! "+ num2);
}

let operation=prompt("Enter operation: ");

console.log(calc(num1,num2,operation));


function calc(num1,num2,operation){
    let result;
    if(operation==='+'){
        result=num1+num2;
    }
    else if(operation==='-'){
        result=num1-num2;
    }
    else if(operation==='*'){
        result=num1*num2;
    }
    else if(operation==='/'){
        result=num1/num2;
    }
    return result;
}